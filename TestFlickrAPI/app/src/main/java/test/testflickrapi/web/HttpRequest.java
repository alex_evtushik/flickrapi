package test.testflickrapi.web;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;

import test.testflickrapi.web.converters.InputStreamConverter;

/**
 * Created by Sasha on 27.01.2015.
 */
public class HttpRequest extends AsyncTask<String, String, String> {

    public static final String TAG = HttpRequest.class.getSimpleName();
    private ActionBarActivity activity;

    public HttpRequest(ActionBarActivity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        Log.d(TAG, "START");
        activity.setSupportProgressBarIndeterminateVisibility(true);
    }

    @Override
    protected String doInBackground(String... params) {
        String result = "";
        try {
            String uri = params[0];
            HttpGet httpGet = new HttpGet(uri);
            HttpResponse response = HttpClientHelper.getHttpClient().execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inStream = entity.getContent();
                result = InputStreamConverter.convertStreamToString(inStream);
                inStream.close();
            }
            return result;
        } catch (IllegalStateException e) {
            return result;
        } catch (ClientProtocolException e) {
            return result;
        } catch (IOException e) {
            return result;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "STOP");
        activity.setSupportProgressBarIndeterminateVisibility(false);
    }
}
