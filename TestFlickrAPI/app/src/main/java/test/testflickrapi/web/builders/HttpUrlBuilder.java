package test.testflickrapi.web.builders;

import java.util.Locale;

import test.testflickrapi.constants.Constants;
import test.testflickrapi.utils.MD5Hash;

/**
 * Created by Sasha on 27.01.2015.
 */
public class HttpUrlBuilder {

    private HttpUrlBuilder() {}

    private static final String HTTP_SERVER = "https://www.flickr.com";
    private static final String HTTP_API_SERVER = "https://api.flickr.com";
    private static final String TEMPLATE_URL_PHOTO_SOURCE = "https://farm%d.staticflickr.com/%s/%s_%s.jpg";
    private static final String API_METHOD_GET_TOKEN = "flickr.auth.getToken";
    private static final String API_METHOD_CHECK_TOKEN = "flickr.auth.checkToken";
    private static final String API_METHOD_GET_LIST_PHOTOSETS = "flickr.photosets.getList";
    private static final String API_METHOD_GET_PHOTOS = "flickr.photosets.getPhotos";
    private static final String TEMPLATE_URL_AUTH = "%s/services/auth/?api_key=%s" +
            "&perms=write&api_sig=%s";
    private static final String TEMPLATE_URL_GET_TOKEN = "%s/services/rest/?method=%s" +
            "&api_key=%s" + "&frob=%s" + "&format=json" + "&nojsoncallback=1" + "&perms=write" + "&api_sig=%s";
    private static final String TEMPLATE_URL_CHECK_TOKEN = "%s/services/rest/?method=%s" + "&api_key=%s" +
            "&format=json" +
            "&nojsoncallback=1" +
            "&perms=write" +
            "&auth_token=%s" + "&api_sig=%s";
    private static final String TEMPLATE_URL_GET_LIST_PHOTOSETS = "%s/services/rest/?method=%s" +
            "&api_key=%s" + "&user_id=%s" + "&format=json" + "&nojsoncallback=1";
    private static final String TEMPLATE_URL_GET_PHOTOS_FROM_PHOTOSET = "%s/services/rest/?method=%s" +
            "&api_key=%s" + "&photoset_id=%s" + "&format=json" + "&nojsoncallback=1";

    public static String getTokenUrl(String frob) {
        String signature = MD5Hash.hashOfString(Constants.API_SECRET + "api_key" + Constants.API_KEY +
            "formatjson" + "frob" + frob + "method" + API_METHOD_GET_TOKEN + "nojsoncallback1" + "permswrite");
        return String.format(Locale.ENGLISH, TEMPLATE_URL_GET_TOKEN, HTTP_API_SERVER, API_METHOD_GET_TOKEN, Constants.API_KEY, frob, signature);
    }

    public static String authUrl() {
        String signature = MD5Hash.hashOfString(Constants.API_SECRET + "api_key" + Constants.API_KEY + "permswrite");
        return String.format(Locale.ENGLISH, TEMPLATE_URL_AUTH, HTTP_SERVER, Constants.API_KEY, signature);
    }

    public static String getListPhotosets(String userId) {
        return String.format(Locale.ENGLISH, TEMPLATE_URL_GET_LIST_PHOTOSETS, HTTP_API_SERVER, API_METHOD_GET_LIST_PHOTOSETS,
                Constants.API_KEY, userId);
    }

    public static String getPhotosFromPhotoset(String photosetId) {
        return String.format(Locale.ENGLISH, TEMPLATE_URL_GET_PHOTOS_FROM_PHOTOSET, HTTP_API_SERVER, API_METHOD_GET_PHOTOS,
                Constants.API_KEY, photosetId);
    }

    public static String getPhotoSource(int farm, String server, String id, String secret) {
        return String.format(Locale.ENGLISH, TEMPLATE_URL_PHOTO_SOURCE, farm, server, id, secret);
    }

    public static String getCheckToken(String token) {
        String signature = MD5Hash.hashOfString(Constants.API_SECRET + "api_key" + Constants.API_KEY + "auth_token" + token +
            "formatjson" + "method" + API_METHOD_CHECK_TOKEN + "nojsoncallback1" + "permswrite" );
        return String.format(Locale.ENGLISH, TEMPLATE_URL_CHECK_TOKEN, HTTP_API_SERVER,
                API_METHOD_CHECK_TOKEN, Constants.API_KEY, token, signature);
    }
}
