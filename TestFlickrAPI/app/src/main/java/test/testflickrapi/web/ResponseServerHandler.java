package test.testflickrapi.web;

import com.google.gson.Gson;

import java.util.ArrayList;

import test.testflickrapi.models.auth.Auth;
import test.testflickrapi.models.auth.ResponseAuth;
import test.testflickrapi.models.photo.Photo;
import test.testflickrapi.models.photo.ResponsePhoto;
import test.testflickrapi.models.photoset.Photoset;
import test.testflickrapi.models.photoset.ResponsePhotosets;

/**
 * Created by Sasha on 27.01.2015.
 */
public class ResponseServerHandler {
    public static final String TAG = ResponseServerHandler.class.getSimpleName();
    private static Gson gson = new Gson();

    public static Auth getAuthData(String res) {
        ResponseAuth resAuth = gson.fromJson(res, ResponseAuth.class);
        if (resAuth.getStat().equalsIgnoreCase("ok"))
            return resAuth.getAuth();
        else return null;
    }

    public static ArrayList<Photoset> getPhotoset(String res) {
        ResponsePhotosets resPhotosets = gson.fromJson(res, ResponsePhotosets.class);
        if (resPhotosets.getStat().equalsIgnoreCase("ok"))
            return resPhotosets.getPhotosets().getPhotoset();
        else return null;
    }

    public static ArrayList<Photo> getPhoto(String res) {
        ResponsePhoto resPhoto = gson.fromJson(res, ResponsePhoto.class);
        if (resPhoto.getStat().equalsIgnoreCase("ok"))
            return resPhoto.getPhotoset().getPhoto();
        return null;
    }
}
