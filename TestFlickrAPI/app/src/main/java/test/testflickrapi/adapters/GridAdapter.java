package test.testflickrapi.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import test.testflickrapi.R;

/**
 * Created by Sasha on 29.01.2015.
 */
public class GridAdapter extends BaseAdapter {
    private ArrayList<String> photosSource = null;
    private LayoutInflater inflater;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;

    public GridAdapter(Context context, ArrayList<String> photosSource) {
        inflater = LayoutInflater.from(context);
        this.photosSource = photosSource;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(android.R.drawable.ic_menu_gallery)
                .showImageOnFail(android.R.drawable.ic_menu_report_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public int getCount() {
        return photosSource.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_grid_photo, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        imageLoader.displayImage(photosSource.get(position), holder.imageView, options);
        return convertView;
    }

    public static class ViewHolder {
        public ImageView imageView;
    }
}
