package test.testflickrapi.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import test.testflickrapi.R;
import test.testflickrapi.ui.preferences.SharedPreferencesHelper;

/**
 * Created by Sasha on 27.01.2015.
 */
public class UserFragment extends Fragment {
    public static final String TAG = UserFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        TextView fullname = (TextView) rootView.findViewById(R.id.fullname);
        fullname.setText(SharedPreferencesHelper.getFullname(getActivity()));
        return rootView;
    }
}
