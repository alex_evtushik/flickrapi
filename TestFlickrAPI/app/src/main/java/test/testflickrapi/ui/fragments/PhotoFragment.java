package test.testflickrapi.ui.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import test.testflickrapi.R;

/**
 * Created by Sasha on 29.01.2015.
 */
public class PhotoFragment extends Fragment {
    public static final String TAG = PhotoFragment.class.getSimpleName();
    private ImageView photoView;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo, container, false);
        photoView = (ImageView) rootView.findViewById(R.id.photo_view);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String source = getArguments().getString("url");
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(android.R.drawable.ic_menu_gallery)
                .showImageOnFail(android.R.drawable.ic_menu_report_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        imageLoader.displayImage(source, photoView, options);
    }
}
