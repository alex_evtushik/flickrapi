package test.testflickrapi.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import test.testflickrapi.R;

/**
 * Created by Sasha on 29.01.2015.
 */
public class ListPhotosetsFragment extends ListFragment {
    public static final String TAG = ListPhotosetsFragment.class.getSimpleName();
    private Callback callback;
    private ArrayList<String> photosetsId = null;
    private ArrayList<String> titles = null;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_photosets, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        photosetsId = args.getStringArrayList("id");
        titles = args.getStringArrayList("title");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titles);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(clickItemLister);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callback = (Callback) activity;
    }

    private OnItemClickListener clickItemLister = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "position = " + position);
            callback.onPhotosetSelected(photosetsId.get(position));
        }
    };


    public interface Callback {
        public void onPhotosetSelected(String id);
    }
}
