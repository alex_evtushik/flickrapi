package test.testflickrapi.ui.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


/**
 * Created by Sasha on 27.01.2015.
 */
public class SharedPreferencesHelper {
    private static final String PREF_FILENAME = "pref";

    public static void setToken(Context context, String token) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        Editor edit = preferences.edit();
        edit.putString(Keys.TOKEN, token);
        edit.commit();
    }

    public static String getToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        return preferences.getString(Keys.TOKEN, "");
    }

    public static void setFullname(Context context, String fullname) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        Editor edit = preferences.edit();
        edit.putString(Keys.FULLNAME, fullname);
        edit.commit();
    }

    public static String getFullname(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        return preferences.getString(Keys.FULLNAME, "");
    }

    public static void setUserID(Context context, String userID) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        Editor edit = preferences.edit();
        edit.putString(Keys.USER_ID, userID);
        edit.commit();
    }

    public static String getUserID(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        return preferences.getString(Keys.USER_ID, "");
    }

    private static class Keys {
        private static final String TOKEN = "token";
        private static final String FULLNAME = "fullname";
        private static final String USER_ID = "user_id";
    }
}
