package test.testflickrapi.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import java.util.ArrayList;

import test.testflickrapi.R;
import test.testflickrapi.adapters.GridAdapter;

/**
 * Created by Sasha on 29.01.2015.
 */
public class GridPhotosGragment extends Fragment {
    public static final String TAG = GridPhotosGragment.class.getSimpleName();
    private Callback callback;
    private ArrayList<String> photosSource = null;
    private GridView gridView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_grid_photos, container, false);
        gridView = (GridView) rootView.findViewById(R.id.grid_view);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        photosSource = args.getStringArrayList("url");
        gridView.setAdapter(new GridAdapter(getActivity(), photosSource));
        gridView.setOnItemClickListener(clickItemLister);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callback = (Callback) activity;
    }

    private OnItemClickListener clickItemLister = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            callback.onPhotoSelected(photosSource.get(position));
        }
    };

    public interface Callback {
        public void onPhotoSelected(String url);
    }
}
