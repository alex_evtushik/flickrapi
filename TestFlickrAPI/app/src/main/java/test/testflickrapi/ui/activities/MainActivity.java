package test.testflickrapi.ui.activities;


import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import test.testflickrapi.R;
import test.testflickrapi.ui.fragments.GridPhotosGragment;
import test.testflickrapi.ui.fragments.ListPhotosetsFragment;
import test.testflickrapi.ui.fragments.LoginFragment;
import test.testflickrapi.ui.fragments.PhotoFragment;
import test.testflickrapi.ui.fragments.UserFragment;
import test.testflickrapi.ui.preferences.SharedPreferencesHelper;
import test.testflickrapi.utils.MD5Hash;
import test.testflickrapi.web.HttpRequest;
import test.testflickrapi.web.ResponseServerHandler;
import test.testflickrapi.web.builders.HttpUrlBuilder;
import test.testflickrapi.models.auth.Auth;
import test.testflickrapi.models.photo.Photo;
import test.testflickrapi.models.photoset.Photoset;


public class MainActivity extends ActionBarActivity implements ListPhotosetsFragment.Callback, GridPhotosGragment.Callback {
    public static final String TAG = MainActivity.class.getSimpleName();
    private String token = "";
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        token = SharedPreferencesHelper.getToken(this);
        if (!token.equalsIgnoreCase("")) {
            String res = "";
            HttpRequest httpRequest = new HttpRequest(this);
            String url = HttpUrlBuilder.getCheckToken(token);
            httpRequest.execute(url);
            try {
                res = httpRequest.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            Auth data = ResponseServerHandler.getAuthData(res);
            checkAuthData(data);
        }
        else {
            fragment = new LoginFragment();
            fragmentTransaction.replace(R.id.container_user_info, fragment);
            fragmentTransaction.commit();

            Uri uri = getIntent().getData();
            if (uri != null) {
                String res = "";
                HttpRequest httpRequest = new HttpRequest(this);
                String url = HttpUrlBuilder.getTokenUrl(uri.getQueryParameter("frob"));
                httpRequest.execute(url);
                try {
                    res = httpRequest.get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                Auth data = ResponseServerHandler.getAuthData(res);
                checkAuthData(data);
            }
        }
    }

    private void showListPhotosets() {
        fragment = new UserFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_user_info, fragment);
        fragmentTransaction.commit();
        getListPhotosets();
    }

    public void login(View v) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HttpUrlBuilder.authUrl())));
    }

    public void logout(View v) {
        SharedPreferencesHelper.setToken(this, "");

        fragment = new LoginFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_user_info, fragment);
        fragmentTransaction.commit();
    }

    private void checkAuthData(Auth data) {
        if (data != null) {
            SharedPreferencesHelper.setToken(this, data.getToken().getContent());
            SharedPreferencesHelper.setUserID(this, data.getUser().getNsid());
            SharedPreferencesHelper.setFullname(this, data.getUser().getFullname());
            showListPhotosets();
        }
        else {
            SharedPreferencesHelper.setToken(this, "");
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HttpUrlBuilder.authUrl())));
        }
    }

    public void getListPhotosets() {
        String userId = SharedPreferencesHelper.getUserID(this);
        String res = "";

        HttpRequest httpRequest = new HttpRequest(this);
        String url = HttpUrlBuilder.getListPhotosets(userId);
        httpRequest.execute(url);
        try {
            res = httpRequest.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        ArrayList<Photoset> photosets = ResponseServerHandler.getPhotoset(res);
        if (photosets != null) {
            Bundle args = new Bundle();
            ArrayList<String> photosetsId = new ArrayList<>();
            ArrayList<String> titles = new ArrayList<>();

            for (Photoset photoset : photosets) {
                photosetsId.add(photoset.getId());
                titles.add(photoset.getTitle().getContent());
            }

            args.putStringArrayList("id", photosetsId);
            args.putStringArrayList("title", titles);

            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.hide(fragment);
            fragmentTransaction.commit();

            fragment = new ListPhotosetsFragment();
            fragment.setArguments(args);
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_data, fragment, ListPhotosetsFragment.TAG);
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentTransaction.commit();
        }

    }

    public void getPhotos(String photosetId) {
        String res = "";
        HttpRequest httpRequest = new HttpRequest(this);
        String url = HttpUrlBuilder.getPhotosFromPhotoset(photosetId);
        httpRequest.execute(url);
        try {
            res = httpRequest.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        ArrayList<Photo> photos = ResponseServerHandler.getPhoto(res);
        if (photos != null) {
            Bundle args = new Bundle();
            ArrayList<String> photosSource = new ArrayList<>();

            int farm;
            String server;
            String id;
            String secret;
            String source;

            for (Photo photo : photos) {
                farm = photo.getFarm();
                server = photo.getServer();
                id = photo.getId();
                secret = photo.getSecret();
                source = HttpUrlBuilder.getPhotoSource(farm,server,id, secret);
                photosSource.add(source);
            }

            args.putStringArrayList("url", photosSource);

            fragment = new GridPhotosGragment();
            fragment.setArguments(args);
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_data, fragment, GridPhotosGragment.TAG);
            fragmentTransaction.addToBackStack(ListPhotosetsFragment.TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onPhotosetSelected(String id) {
        getPhotos(id);
    }

    @Override
    public void onPhotoSelected(String url) {
        Bundle args = new Bundle();
        args.putString("url", url);
        fragment = new PhotoFragment();
        fragment.setArguments(args);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_data, fragment, PhotoFragment.TAG);
        fragmentTransaction.addToBackStack(GridPhotosGragment.TAG);
        fragmentTransaction.commit();
    }
}
