package test.testflickrapi.models.auth;

/**
 * Created by Sasha on 27.01.2015.
 */
public class User {
    private String username;
    private String nsid;
    private String fullname;

    public String getUsername() {
        return username;
    }

    public String getNsid() {
        return nsid;
    }

    public String getFullname() {
        return fullname;
    }
}
