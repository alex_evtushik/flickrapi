package test.testflickrapi.models.photo;

/**
 * Created by Sasha on 29.01.2015.
 */
public class Photo {
    private String id;
    private String secret;
    private String server;
    private int farm;
    private String title;
    private String isprimary;
    private int ispublic;
    private int friend;
    private int isfamily;

    public String getId() {
        return id;
    }

    public String getSecret() {
        return secret;
    }

    public String getServer() {
        return server;
    }

    public int getFarm() {
        return farm;
    }

    public String getTitle() {
        return title;
    }

    public String getIsprimary() {
        return isprimary;
    }

    public int getIspublic() {
        return ispublic;
    }

    public int getFriend() {
        return friend;
    }

    public int getIsfamily() {
        return isfamily;
    }
}
