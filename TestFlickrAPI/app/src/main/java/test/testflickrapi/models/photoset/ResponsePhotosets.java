package test.testflickrapi.models.photoset;

/**
 * Created by Sasha on 28.01.2015.
 */
public class ResponsePhotosets {
    private Photosets photosets;
    private String stat;

    public Photosets getPhotosets() {
        return photosets;
    }

    public String getStat() {
        return stat;
    }
}
