package test.testflickrapi.models.photo;

import java.util.ArrayList;

/**
 * Created by Sasha on 29.01.2015.
 */
public class Photoset {
    private String id;
    private String primary;
    private String owner;
    private String ownername;
    private ArrayList<Photo> photo;
    private int page;
    private int per_page;
    private int perpage;
    private int pages;
    private String total;
    private String tittle;

    public String getId() {
        return id;
    }

    public String getPrimary() {
        return primary;
    }

    public String getOwner() {
        return owner;
    }

    public String getOwnername() {
        return ownername;
    }

    public ArrayList<Photo> getPhoto() {
        return photo;
    }

    public int getPage() {
        return page;
    }

    public int getPer_page() {
        return per_page;
    }

    public int getPerpage() {
        return perpage;
    }

    public int getPages() {
        return pages;
    }

    public String getTotal() {
        return total;
    }

    public String getTittle() {
        return tittle;
    }
}
