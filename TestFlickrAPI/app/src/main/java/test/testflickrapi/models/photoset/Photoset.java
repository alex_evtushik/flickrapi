package test.testflickrapi.models.photoset;

/**
 * Created by Sasha on 29.01.2015.
 */
public class Photoset {
    private String id;
    private String primary;
    private String secret;
    private String server;
    private int farm;
    private String photos;
    private int videos;
    private Title title;
    private Description description;
    private int needs_interstitital;
    private int visibility_can_see_set;
    private String count_views;
    private String comments;
    private int can_comment;
    private String date_create;
    private String date_update;

    public String getId() {
        return id;
    }

    public String getPrimary() {
        return primary;
    }

    public String getSecret() {
        return secret;
    }

    public String getServer() {
        return server;
    }

    public int getFarm() {
        return farm;
    }

    public String getPhotos() {
        return photos;
    }

    public int getVideos() {
        return videos;
    }

    public Title getTitle() {
        return title;
    }

    public Description getDescription() {
        return description;
    }

    public int getNeedsInterstitital() {
        return needs_interstitital;
    }

    public int getVisibility() {
        return visibility_can_see_set;
    }

    public String getCountViews() {
        return count_views;
    }

    public String getComments() {
        return comments;
    }

    public int getCanComment() {
        return can_comment;
    }

    public String getDateCreate() {
        return date_create;
    }

    public String getDateUpdate() {
        return date_update;
    }
}
