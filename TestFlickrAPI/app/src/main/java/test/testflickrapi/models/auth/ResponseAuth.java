package test.testflickrapi.models.auth;

/**
 * Created by Sasha on 28.01.2015.
 */
public class ResponseAuth {
    private Auth auth;
    private String stat;

    public Auth getAuth() {
        return auth;
    }

    public String getStat() {
        return stat;
    }
}
