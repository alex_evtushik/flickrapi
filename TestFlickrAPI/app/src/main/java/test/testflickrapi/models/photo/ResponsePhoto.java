package test.testflickrapi.models.photo;

/**
 * Created by Sasha on 29.01.2015.
 */
public class ResponsePhoto {
    private Photoset photoset;
    private String stat;

    public Photoset getPhotoset() {
        return photoset;
    }

    public String getStat() {
        return stat;
    }
}
