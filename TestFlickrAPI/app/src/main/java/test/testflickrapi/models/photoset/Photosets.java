package test.testflickrapi.models.photoset;

import java.util.ArrayList;

/**
 * Created by Sasha on 29.01.2015.
 */
public class Photosets {
    private int cancreate;
    private int page;
    private int pages;
    private int perpage;
    private int total;
    ArrayList<Photoset> photoset;

    public int getCancreate() {
        return cancreate;
    }

    public int getPage() {
        return page;
    }

    public int getPages() {
        return pages;
    }

    public int getPerpage() {
        return perpage;
    }

    public int getTotal() {
        return total;
    }

    public ArrayList<Photoset> getPhotoset() {
        return photoset;
    }
}
