package test.testflickrapi.models.auth;

/**
 * Created by Sasha on 27.01.2015.
 */
public class Auth {

    private Token token;
    private Perms perms;
    private User user;

    public Auth() {
    }

    public Token getToken() {
        return token;
    }

    public Perms getPerms() {
        return perms;
    }

    public User getUser() {
        return user;
    }
}
